package testzadanieSpecyfikacja;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import zadanieSpecyfikacja.WiekOsoby;

import static org.junit.Assert.assertEquals;

public class WiekOsobyTest {

    @Rule
    public ExpectedException exp = ExpectedException.none();

    @Test
    public void dataUrodzeniaWiekszaOdWybranegoDniaZwracaWyjatek(){
        exp.expect(IllegalArgumentException.class);
        exp.expectMessage("Nieprawidłowy dobór dat");
        WiekOsoby.obliczWiek("03-02-2000","02-02-2000");
    }

    @Test
    public void obecnyWiekWynosi20Lat(){
        assertEquals(20,WiekOsoby.obliczWiek("01-01-1990", "01-01-2010"));
    }
    @Test
    public void obecnyWiekWynosi59Lat(){
        assertEquals(59,WiekOsoby.obliczWiek("02-01-1950", "01-01-2010"));
    }
    @Test
    public void obecnyWiekWynosi0Lat(){
        assertEquals(0,WiekOsoby.obliczWiek("02-01-1950", "02-01-1950"));
    }







}
