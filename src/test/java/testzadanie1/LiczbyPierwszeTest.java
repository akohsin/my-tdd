package testzadanie1;
import org.junit.Assert;

import org.junit.Test;
import zadanie1.LiczbyPierwsze;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LiczbyPierwszeTest {
    @Test
    public void siedemJestLiczbaPierwsza() {
        assertTrue(LiczbyPierwsze.czyLiczbaPierwsza(7));
    }
    @Test
    public void osiemNieJestLiczbaPierwsza() {
        assertFalse(LiczbyPierwsze.czyLiczbaPierwsza(8));
    }
    @Test
    public void jedenNieJestLiczbaPierwsza() {
        assertFalse(LiczbyPierwsze.czyLiczbaPierwsza(1));
        }
    @Test
    public void dziewiecNieJestLiczbaPierwsza() {
        assertFalse(LiczbyPierwsze.czyLiczbaPierwsza(9));
    }
    @Test
    public void zeroNieJestLiczbaPierwsza() {
        assertFalse(LiczbyPierwsze.czyLiczbaPierwsza(0));
    }
}
