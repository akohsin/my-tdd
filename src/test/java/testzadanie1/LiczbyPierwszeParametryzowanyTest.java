package testzadanie1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import zadanie1.LiczbyPierwsze;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class LiczbyPierwszeParametryzowanyTest {
    @Parameterized.Parameter
    public int liczba;
    @Parameterized.Parameter(value = 1)
    public boolean czyPierwsza;

    @Parameterized.Parameters(name = "{index}:czy liczba {0} powinna byc pierwsza? {1}")
    public static Collection<Object[]> dataProvider() {
        return Arrays.asList(new Object[][]{
                {1, false},
                {7, true},
                {8, false},
                {9, false},
                {0, false},
                {-2, false},
                {13, true}});
    }
    @Test
    public void czyLiczbaPierwszaTest(){
        assertEquals(czyPierwsza, LiczbyPierwsze.czyLiczbaPierwsza(liczba));
    }

}



