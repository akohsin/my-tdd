package testzadanie3;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import zadanie3.KartaKlienta;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class DobreWinoStalyKlientParametryzowanyTest {

    public KartaKlienta karta;

    public double kwota;
    public int liczbaKuponow;

    public DobreWinoStalyKlientParametryzowanyTest(double kwota, int liczbaKuponow) {
        this.kwota = kwota;
        this.liczbaKuponow = liczbaKuponow;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> dataProvider() {
        return Arrays.asList(new Object[][]{
                {41.23d, 10},
                {41.24d, 11},
                {86.95d, 11},
                {86.96d, 12},
                {137.93d, 12},
                {137.94d, 13},
                {200.00d, 13}});
    }

    @Before
    public void utworzKarte() {
        karta = new KartaKlienta();
    }

    @Test
    public void zrobZakupTest() {
        karta.zrobZakup(200.00d);
        karta.zrobZakup(200.00d);
        karta.zrobZakup(200.00d);
        karta.zrobZakup(50.00d);
        karta.zrobZakup(kwota);
        assertEquals(liczbaKuponow, karta.pobierzLiczbeKuponow());
    }

}


