package testzadanie3;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import zadanie3.KartaKlienta;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class DobreWinoParametryzowanyTest {

    public KartaKlienta karta;

    public double kwota;
    public int liczbaKuponow;

    public DobreWinoParametryzowanyTest(double kwota, int liczbaKuponow) {
        this.kwota = kwota;
        this.liczbaKuponow = liczbaKuponow;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> dataProvider() {
        return Arrays.asList(new Object[][]{
                {39.99d, 0},
                {40.00d, 1},
                {84.21d, 1},
                {84.22d, 2},
                {133.33d, 2},
                {133.34d, 3},
                {200.00d, 3}});
    }

    @Before
    public void utworzKarte() {
        karta = new KartaKlienta();
    }

    @Test
    public void zrobZakupTest() {
        karta.zrobZakup(kwota);
        assertEquals(liczbaKuponow, karta.pobierzLiczbeKuponow());
    }

}



