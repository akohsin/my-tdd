package testzadanie3;

import org.junit.Before;
import org.junit.Test;
import zadanie3.KartaKlienta;

import static org.junit.Assert.assertEquals;

public class DobreWinoTest {
    KartaKlienta karta;

    @Before
    public void utwórzKarte() {
        karta = new KartaKlienta();
    }

    @Test
    public void zakupPonizej40Daje0Bonow() {
        karta.zrobZakup(39.99d);
        assertEquals(0, karta.pobierzLiczbeKuponow());
    }

    @Test
    public void zakupPowyzej40Daje1Bon() {
        karta.zrobZakup(40.00d);
        assertEquals(1, karta.pobierzLiczbeKuponow());
    }
}