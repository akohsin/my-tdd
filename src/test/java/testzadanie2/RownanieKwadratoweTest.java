package testzadanie2;

        import org.junit.Test;
        import org.junit.runner.RunWith;
        import org.junit.runners.Parameterized;
        import zadanie2.RownanieKwadratowe;

        import java.util.Arrays;
        import java.util.Collection;

        import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class RownanieKwadratoweTest {
    @Parameterized.Parameter(value=0)
    public int a;

    @Parameterized.Parameter(value = 1)
    public int b;

    @Parameterized.Parameter(value=2)
    public int c;

    @Parameterized.Parameter(value = 3)
    public String wynik;

    @Parameterized.Parameters (name = "[{index}] {0}x^2 + {1}x +{2} | wynik: [{3}]")
    public static Collection<Object[]> dataProvider() {
        return Arrays.asList(new Object[][]{
                {10, 1, 1, ""},
                {1,2,1, "-1"},
                {1,-3,-4, "-1,4"}});
    }

    @Test
    public void rownanieKwadratoweTest(){
        assertEquals(wynik,
                RownanieKwadratowe.obliczPierwiastki(a,b,c));
    }
}