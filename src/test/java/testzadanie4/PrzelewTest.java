package testzadanie4;

import org.junit.Before;
import org.junit.Test;
import zadanie4.KontoBankowe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PrzelewTest {
    KontoBankowe kontoAdama;
    KontoBankowe kontoEwy;

    @Before
    public void przygotujKonto() {
        kontoAdama = new KontoBankowe(1234, 1000, 0);
        kontoEwy = new KontoBankowe(5555, 1000, 10000);
    }
    @Test
    public void przelejKwoteDlaSpelnionychWarunkow(){
        assertTrue(kontoEwy.przelej(kontoAdama, 500,5555));
    }
    @Test
    public void sprawdzCzyPrzelewDoszedl(){
        kontoEwy.przelej(kontoAdama,500,5555);
        assertEquals(500, kontoAdama.sprawdzSaldo());
    }
    @Test
    public void sprawdzCzyPrzelewWyszedl(){
        kontoEwy.przelej(kontoAdama,500,5555);
        assertEquals(9500, kontoEwy.sprawdzSaldo());
    }


}
