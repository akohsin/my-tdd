package zadanie4;

public class KontoBankowe {
    private int pin;
    private int limitDzienny;
    private int stanKonta;

    public KontoBankowe(int pin, int limitDzienny, int stanKonta) {
        this.pin = pin;
        this.limitDzienny = limitDzienny;
        this.stanKonta = stanKonta;
    }
    public boolean przelej(KontoBankowe kontoBankowe, int kwota, int pin){
        if (pin==this.pin&&kwota<=limitDzienny&&kwota<=stanKonta){
            stanKonta-=kwota;
            kontoBankowe.wplacSrodki(kwota);
            return true;
        }
        else return false;
    }

    private void wplacSrodki(int kwota) {
    stanKonta+=kwota;
    }
    public int sprawdzSaldo(){
        return stanKonta;
    }

}
