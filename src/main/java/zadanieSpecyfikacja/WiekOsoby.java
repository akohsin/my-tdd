package zadanieSpecyfikacja;


import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class WiekOsoby {

    /**
     * Oblicza wiek osoby w danym, wybranym dniu
     *
     * @param dataUrodzenia - wymagany format "dd-MM-yyyy" np.:"27-10-1994"
     * @param wybranaData   - wymagany format "dd-MM-yyyy" np.:"27-10-1994"
     * @return wiek osoby obliczony dla danego dnia
     * <p>
     * Wymaga dataUrodzenia< wybranaData
     * @throws IllegalArgumentException
     */
    public static int obliczWiek(String dataUrodzenia, String wybranaData) {
        int wiek=0;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        try {
            LocalDate dataUr = LocalDate.parse(dataUrodzenia, dtf);
            LocalDate wybrana = LocalDate.parse(wybranaData, dtf);
            if (wybrana.isBefore(dataUr)){
                throw new IllegalArgumentException("Nieprawidłowy dobór dat");
            }
            wiek = Period.between(dataUr, wybrana).getYears();
        }catch (DateTimeParseException dtpe){
            throw new IllegalArgumentException("Nieprawidłowy dobór dat");
        }

        return wiek;
    }
}
