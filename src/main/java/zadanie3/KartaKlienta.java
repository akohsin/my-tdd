package zadanie3;

public class KartaKlienta {
    private int liczbaBonowKonto = 0;
    public void zrobZakup(double cena){
        double cenaZeZnizka=0;
        double znizka=0;
        if (liczbaBonowKonto>=10){
            znizka=0.03;
        }
        if (cena>100){
           znizka+=0.1;
        }
        else if (cena>50){
            znizka+=0.05;
        }

        cenaZeZnizka=cena*(1-znizka);

        int liczbaBonow=(int)cenaZeZnizka/40;
        if (liczbaBonow>3)liczbaBonow=3;
        liczbaBonowKonto+=liczbaBonow;
    }
    public int pobierzLiczbeKuponow(){
        return liczbaBonowKonto;
    }
}
