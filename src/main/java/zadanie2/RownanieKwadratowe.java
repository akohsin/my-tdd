package zadanie2;


public class RownanieKwadratowe {

    private static final String SEPARATOR_PIERWIASTKOW = ",";

    public static String obliczPierwiastki(int a, int b, int c) {
        double delta = obliczDelte(a, b, c);
        String pierwiastki = "";
        if (delta >= 0.0) {
            double pierwiastekDelty = Math.sqrt(delta);
            int x1 = (int) (-b - pierwiastekDelty) / 2 * a;
            int x2 = (int) (-b + pierwiastekDelty) / 2 * a;
            pierwiastki += x1;
            if (x1 != x2) {
                pierwiastki += SEPARATOR_PIERWIASTKOW + x2;
            }
        }
        return pierwiastki;
    }

    private static double obliczDelte(int a, int b, int c) {
        return b * b - 4 * a * c;
    }
}
